using System;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;

namespace GenerateStrings.NUnit
{
    [TestFixture]
    internal sealed class StringGeneratorFixture
    {
        [Test, Repeat(1000)]
        public void StringIsGeneratedUsingLegalCharacters()
        {
            var r = new Random();
            var legalCharacters = StringGenerator.AdmittedChars.ToHashSet();
            var randomString = StringGenerator.GenerateString(r).ToCharArray();

            Assert.That(randomString.Length, Is.EqualTo(8));
            Assert.That(randomString.All(c => legalCharacters.Contains(c)));
        }

        [Test]
        public void BatchOfStringsCanBeGenerated()
        {
            var r = new Random();
            var batch = StringGenerator.GenerateBatchOfStrings(r, 100);
            Assert.That(batch.Count, Is.EqualTo(100));
        }

        [Test]
        public async Task StringBatchesCanBeCreatedAsynchronously()
        {
            var r = new Random();
            var batch = await StringGenerator.GenerateBatchOfStringsAsync(r, 100);
            Assert.That(batch.Count, Is.EqualTo(100));
        }
    }
}